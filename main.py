import os
import datetime
import requests


class ApiRequest:

    def __init__(self, url):
        self.url = url

    def getting_response(self):
        return requests.get(self.url)

    def getting_library(self):
        return self.getting_response().json()


class ReportCreating:

    def __init__(self, url_users, url_todos):
        self.request_users = ApiRequest(url_users)
        self.response_users = self.request_users.getting_library()
        self.request_todos = ApiRequest(url_todos)
        self.response_todos = self.request_todos.getting_library()

    def directory_creating(self):
        if os.path.exists("tasks"):
            self.files_creating()
        else:
            os.mkdir("tasks")
            self.files_creating()

    def files_creating(self):
        if self.request_users.getting_response() and self.request_todos.getting_response():
            file_path = os.path.realpath(__file__)
            script_dir = os.path.dirname(file_path)

            for user in self.response_users:
                username = user["username"]
                if os.path.exists(fr"{script_dir}\tasks\{username}.txt"):
                    with open(fr"{script_dir}\tasks\{username}.txt", 'r', encoding="utf-8") as read:
                        read.readline()
                        date = read.readline()
                        date = date[-17:len(date) - 1]
                        date = date.replace(" ", "T")
                        date = date.replace(".", "-")
                        date = date.replace(":", "-")
                        read.close()
                        os.rename(fr"{script_dir}\tasks\{username}.txt",
                                  fr"{script_dir}\tasks\old_{username}_{date}.txt")
                with open(fr"{script_dir}\tasks\{username}.txt", 'w', encoding="utf-8") as write:
                    self.report_writing(write, user)
                write.close()

    def report_writing(self, file, user):
        today_date = datetime.datetime.now()
        day = today_date.day
        month = today_date.month
        hour = today_date.hour
        minute = today_date.minute
        if day < 10:
            day = f"0{day}"
        if month < 10:
            month = f"0{month}"
        if hour < 10:
            hour = f"0{hour}"
        if minute < 10:
            minute = f"0{minute}"
        today_date = "{}.{}.{} {}:{}".format(day, month, today_date.year, hour, minute)
        tasks = []

        count_completed = 0
        count_uncompleted = 0
        completed = ""
        uncompleted = ""
        for todo in self.response_todos:
            try:
                if todo["userId"] == user["id"]:
                    tasks.append(todo)
                    if todo["completed"]:
                        count_completed += 1
                        if len(todo["title"]) > 46:
                            completed += f"- {todo['title'][:46]}...\n"
                        else:
                            completed += f"- {todo['title']}\n"
                    else:
                        count_uncompleted += 1
                        if len(todo["title"]) > 46:
                            uncompleted += f"- {todo['title'][:46]}...\n"
                        else:
                            uncompleted += f"- {todo['title']}\n"
            except:
                continue

        file.write(f"# Отчёт для {user['company']['name']}.\n"
                   f"{user['name']} <{user['email']}> {today_date}\n"
                   f"Всего задач: {len(tasks)}\n\n"
                   f"## Актуальные задачи ({count_completed}):\n"
                   f"{completed}\n"
                   f"## Завершённые задачи ({count_uncompleted}):\n"
                   f"{uncompleted}")


if __name__ == '__main__':
    report = ReportCreating("https://json.medrocket.ru/users", "https://json.medrocket.ru/todos")
    report.directory_creating()

